package com.example.apicachingdemo.model;

import lombok.Data;

import java.util.Map;

@Data
public class MasterData {

    Map<String, String> data;
}
