package com.example.apicachingdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApicachingdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApicachingdemoApplication.class, args);
	}
}
