package com.example.apicachingdemo;

import com.example.apicachingdemo.model.MasterData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;
import java.util.TreeMap;

@RestController
public class CachingController {

    @GetMapping(value = "/master/lead")
    public ResponseEntity<Object> getMasterData(WebRequest request) {
        int randomInt = (int)(Math.random()*((10-1)+1))+1;
        System.out.println("Random no is "+randomInt);
        MasterData masterData = new MasterData();
        Map<String, String> dataMap = new TreeMap<>();
        String etTag;
        if(randomInt %2 == 0) {
            etTag = "even";
            System.out.println("Even");
        } else {
            etTag = "odd";
            System.out.println("Odd");
        }
        dataMap.put("1", "First");
        if(request.checkNotModified(etTag)) {
            return null;
        }
        masterData.setData(dataMap);
        return new ResponseEntity<>(masterData, HttpStatus.OK);
    }
}
